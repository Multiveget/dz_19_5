#include <iostream>
#include <string>


class Animal
{
 public:
	virtual void Voice()
	{
		std::cout << "Animal class was created";
	}
};

class Dog : public Animal
{ 
 public:
    void Voice() override
	{
		std::cout << "Woof!"<< '\n';
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meaw!" << '\n';
	}
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "My!" << '\n';
	}
};


int main()
{
	Animal* zoo[3];// ������ ����������
	Dog sobaka;
	Cat koshka;
	Cow korowa;
	zoo[0] = &sobaka;
	zoo[1] = &koshka;
	zoo[2] = &korowa;
	for (auto& pets : zoo)
	{
		pets->Voice();
	}
}


